
## Seconde partie du projet

### TÂCHE 9 : Ajout d'une méthode `listaAllFiles`

Dans la classe `MusicOrganizer`, vous devez ajouter une méthode
de signature `public void listAllFiles()`, qui doit afficher
dans la `console` la liste des pistes.

Cette méthode devra utiliser l'instruction d'itération :

```java
for (Type objet : collection) {
  // Instructions utilisant objet
}
```

Cette instruction permet de parcourir tous les éléments d'une collection.

1. Créez la méthode `listAllFiles`
2. Testez le comportement de cette méthode en ajoutant plusieurs fichiers.
3. N'oubliez pas d'enregistrer votre travail avec la commande `git commit`
4. Modifiez `listAllFiles` pour ajouter la position du fichier dans la liste,
exemple de sortie :

```
01 : Le printemps
02 : L'été
03 : L'automne
04 : L'hiver
...
```

5. Testez et *committez*.


### TÂCHE 10 : Ajout d'une méthode `listMatching`

Dans la classe `MusicOrganizer`, vous devez ajouter une méthode
de signature `public void listMatching(String searchString)`, qui doit afficher
dans la `console` toutes les pistes qui ont le mot passé en
paramètre dans leur titre.


1. Créez la méthode `listMatching`
2. Testez le comportement de cette méthode en ajoutant plusieurs fichiers.
3. N'oubliez pas d'enregistrer votre travail avec la commande `git commit`.
4. Modifiez `listMatching` pour ajouter la position du fichier dans la liste,
exemple de sortie :

```
01 : Le printemps
02 : L'été
03 : L'automne
04 : L'hiver
...
```

5. Testez et *committez*.
6. Ajoutez un message si aucun titre ne correspond au texte recherché.
7. Modifiez la méthode pour retourner la liste des titres sélectionnés
7. Créez une méthode `playTracksSamples`, qui joue un extrait de chacun des titres
passés en paramètre.
8. Ecrivez une méthode `playMatchingSamples(String searchString)`, qui joue
un extrait des chansons trouvées par la `searchString`, un petit `jingle` par
défaut sera joué si aucun titre n'est trouvé (à vous de chercher un jingle sur
  Internet)
9. Testez et *committez*
10. Synchronisez sur le serveur `BitBucket`: `git push --all`

### TÂCHE 11 : Utilisation d'objets `Track` pour les pistes

Nous allons maintenant compléter le programme finale pour faire un peu plus
de gestion de pistes. Pour cela nous allons utiliser la version `v5` de `music-player`

1. Quittez BlueJ

1. Créez une nouvelle branche :

  `git checkout -b mon-tp-v5 v5`

1. Ouvrez le projet `music-organizer` dans BlueJ.

Dans cette nouvelle version, il y a deux nouvelles classes :

    - `TrackReader` : permet de lire tous les fichiers dans un dossier
    - `Track` : classe utilisée pour représenter un morceau de musique.

Une première amélioration de la classe `MusicPlayer` :

1. Ajoutez un constructeur à la classe `MusicPlayer` pour pouvoir passer
le dossier des fichiers musicaux en paramètre.
1. Modifiez le constructeur initial pour prendre en compte ce nouveau constructeur
et éviter la duplication de code
1. Testez et *committez*


Votre second travail, consiste à ajouter un compteur du nombre de fois
où un morceau a été joué.

1. Dans la classe `Track` ajoutez un attribut `playCount`
1. Ajoutez les méthodes nécessaires à l'utilisation de cet attribut (accesseur et mutateur)
1. Modifiez la méthode `getDetails`
1. Testez et `committez`
1. Dans `MusicOrganizer` ajoutez l'incrément du compteur à chaque fois qu'un morceau est joué

Quand un second morceau de musique est joué pendant qu'un premier est en cours,
les deux sont joués simultanément.

1. Modifiez le programme pour stopper le premier morceau quand vous en jouez un nouveau

La plupart des lecteurs de musique ont une fonction pour jouer les morceaux
de musique dans un ordre aléatoire, votre lecteur doit avoir cette fonction :

1. ajoutez une méthode `randomPlay`qui permet de jouer de manière aléatoire la liste
des pistes
1. ajoutez une méthode de même nom qui prend une liste de pistes àa jouer en paramètres,
si cette liste  est vide on prendra la liste complète des titres
1. Testez et `committez`

### TÂCHE 12 Suppression de pistes

La majorité des logiciels de musique permettent de supprimer des morceaux, vous
allez donc ajouter cette fonctionnalité.

Pour faire cela vous allez un objet spécial associé aux collection
appelé `Iterator` (http://docs.oracle.com/javase/8/docs/api/java/util/Iterator.html).
Ne manquez pas de lire cette documentation.

La classe `Iterator<E>` est en fait une classe **interface**, qui ne permet pas
de créer des objets mais qui définit certaines méthodes que devront implémenter
les classes **concrètes** l'utilisant. Nous y reviendrons plus tard.

Le corps de votre méthode devra contenir un équivalent de cette suite
d'instructions, qui permet de supprimer les objets d'une collection :

```java
Iterator<Track> it = tracks.iterator();
while (it.hasNext()) {
  Track t = it.next();
  String artist = t.getArtist();
  if (artist.equals(artistToRemove)) {
    it.remove();
  }
}
```

1. Ecrire la méthode `removeArtistTrack`, qui supprimera les morceaux d'un artiste
dont le nom est passé en paramètre.
1. Testez et `committez`
1. Synchronisez votre travail sur *Bitbucket* : `git push --all`
